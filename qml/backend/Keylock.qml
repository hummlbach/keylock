import QtQuick 2.6
import io.thp.pyotherside 1.3

Python {
    id: keylock

    signal encryptionSuccess(string encryptedFile)
    signal decryptionSuccess(string decryptedFile)
    signal importSuccess()
    signal pyError(string errorMessage)

    property var list: null
    property var currentKey: null
    property string encryptedFile: ""
    property string decryptedFile: ""
    property string passphrase: ""

    property bool _ongoingEncryption: false
    property bool _ongoingDecryption: false

    function generateKey(email, passphrase) {
    /* TODO: the name overloading here is a bit confusing =) */
        keylock.call('keylock.generate_key', [email, passphrase], onKeyGenerated);
    }

    function onKeyGenerated(keylist) {
        currentKey = keylist[keylist.length-1];
        listKeys(keylist);
    }

    function listKeys(keyList) {
        keylock.list = keyList;
    }

    function removeKey(key) {
        keylock.call('keylock.remove_key', [key.fingerprint], listKeys);
    }

    onEncryptedFileChanged: {
        if (encryptedFile != null && encryptedFile != "" && !_ongoingEncryption) {
            _ongoingDecryption = true;
            keylock.call('keylock.decrypt', [encryptedFile, passphrase], onDecrypted);
        }
    }
    
    function onDecrypted(filename) {
        if (filename == null || filename == "") {
            _ongoingDecryption = false;
            return;
        }
        decryptedFile = filename;
        _ongoingDecryption = false;
        decryptionSuccess(filename);
    }

    onDecryptedFileChanged: {
        if (decryptedFile != null && decryptedFile != "" && !_ongoingDecryption) {
            _ongoingEncryption = true;
            keylock.call('keylock.encrypt', [decryptedFile, currentKey], onEncrypted);
        }
    }

    function onEncrypted(filename) {
        if (filename == null || filename == "") {
            _ongoingEncryption = false;
            return;
        }
        encryptedFile = filename;
        _ongoingEncryption = false;
        encryptionSuccess(filename);
    }

    function importKey(filename) {
        keylock.call('keylock.import_key', [filename], onKeyImported);
    }

    function onKeyImported(keylist) {
        if (keylist == null) {
            return;
        }
        currentKey = keylist[keylist.length-1];
        listKeys(keylist);
        importSuccess();
    }

    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('../../py'));
        importModule('keylock', onModuleImported);
        setHandler('error', pyError);
    }

    function onModuleImported() {
        keylock.call('keylock.list_keys', [], listKeys);
    }

    function isKeyfile(filename) {
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        return (ext == 'asc');
    }

    function isEncrypted(filename) {
        return keylock.call_sync('keylock.is_encrypted', [filename]);
    }

    function getKeyByEmail(email) {
        return keylock.call_sync('keylock.get_key_by_email', [email]);
    }

    onError: {
        console.log('python error: ' + traceback);
    }
}

