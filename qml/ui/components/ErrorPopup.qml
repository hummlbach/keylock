import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups

UITK_Popups.Dialog {
    id: monolog
    objectName: "errorPopup"

    property var closeButtonText: i18n.tr("Argl!")
    property var closeButtonColor: UITK.UbuntuColors.ash

    signal closed();

    text: monolog.text

    UITK.Button {
        text: monolog.closeButtonText
        color: monolog.closeButtonColor
        onClicked: {
            monolog.closed();
            UITK_Popups.PopupUtils.close(monolog)
        }
    }
}
