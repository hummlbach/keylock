import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "../delegates"
import "../components"

Page {
    id: keyListPage

    header: UITK.PageHeader {
        id: header
        title: i18n.tr('Enigma')

        trailingActionBar.actions: [
            UITK.Action {
                id: addKey
                objectName: "addKey"
                text: i18n.tr("Add key")
                iconName: "add"
                onTriggered: {
                    UITK_Popups.PopupUtils.open(addKeyDialog)
                }
            }
        ]
    }

    ScrollView {
        anchors.fill: parent

        ListView {
            id: keyListView
            anchors.fill: parent

            model: keylock.list

            delegate: KeyListItem {
                key: modelData
                onRemoveKey: {
                    UITK_Popups.PopupUtils.open(removeConfirmationComponent)
                }

               Component {
                    id: removeConfirmationComponent
                    ConfirmationPopup {
                        onConfirmed: keylock.removeKey(key)
                        text: i18n.tr("Do you really want to delete the key for %1?").arg(key.uids.toString())
                        confirmButtonText: i18n.tr("Delete")
                        confirmButtonColor: UITK.UbuntuColors.red
                    }
                }
            }
        }
    }

    Item {
		id: welcomeItem

		anchors.top: parent.top
		anchors.topMargin: units.gu(10)
 
		visible: keyListView.count === 0

		width: parent.width
		height: parent.height - header.height

		Column {
			width: parent.width
			height: parent.height
			spacing: units.gu(2)

			anchors.horizontalCenter: parent.horizontalCenter

			UITK.UbuntuShape {
				width: units.gu(34.19); height: units.gu(15)
				anchors.horizontalCenter: parent.horizontalCenter
				radius: "medium"
				image: Image {
				source: Qt.resolvedUrl("../../../assets/logo.svg")
				}
			}

			Label {
				width: parent.width
				horizontalAlignment: Text.AlignHCenter
				font.pointSize: units.gu(2)
				text: i18n.tr("Start by adding PGP keys")
			}

			Label {
				width: parent.width
				horizontalAlignment: Text.AlignHCenter
				text: i18n.tr("Click the '+' at the top to generate a new key or\n import your keyring by sharing an asc file to Enigma.")
			}
		}
    }

    AddKeyDialog {
        id: addKeyDialog
    }

}
